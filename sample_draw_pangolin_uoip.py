import sys
sys.path.append('/Users/lukaszpluzynski/Pangolin/build/src')
import OpenGL.GL as gl
import pypangolin
import numpy as np

def DrawCamera(camera,w,h_ratio,z_ratio):

    h = w * h_ratio
    z = w * z_ratio

    gl.glPushMatrix()
    gl.glMultTransposeMatrixd(camera)

    gl.glBegin(gl.GL_LINES)

    gl.glVertex3f(0,0,0)
    gl.glVertex3f(w,h,z)
    gl.glVertex3f(0,0,0)
    gl.glVertex3f(w,-h,z)
    gl.glVertex3f(0,0,0)
    gl.glVertex3f(-w,-h,z)
    gl.glVertex3f(0,0,0)
    gl.glVertex3f(-w,h,z)

    gl.glVertex3f(w,h,z)
    gl.glVertex3f(w,-h,z)
    gl.glVertex3f(-w,h,z)
    gl.glVertex3f(-w,-h,z)
    gl.glVertex3f(-w,h,z)
    gl.glVertex3f(w,h,z)
    gl.glVertex3f(-w,-h,z)
    gl.glVertex3f(w,-h,z)

    gl.glEnd()
    gl.glPopMatrix()

def main():
    pypangolin.CreateWindowAndBind('Main', 640, 480)
    gl.glEnable(gl.GL_DEPTH_TEST)

    # Define Projection and initial ModelView matrix
    scam = pypangolin.OpenGlRenderState(
        pypangolin.ProjectionMatrix(640, 480, 420, 420, 320, 240, 0.2, 200),
        pypangolin.ModelViewLookAt(-2, 2, -2, 0, 0, 0, pypangolin.AxisDirection.AxisY))
    handler = pypangolin.Handler3D(scam)

    # Create Interactive View in window
    dcam = pypangolin.CreateDisplay()
    dcam.SetBounds(pypangolin.Attach(0), pypangolin.Attach(1), pypangolin.Attach.Pix(100), pypangolin.Attach(1), -640.0/480.0)
    dcam.SetHandler(handler)


    trajectory = [[0, -6, 6]]
    for i in range(300):
        trajectory.append(trajectory[-1] + np.random.random(3)-0.5)
    trajectory = np.array(trajectory)
    i=0
    while not pypangolin.ShouldQuit():
        i+=0.01
        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
        gl.glClearColor(1.0, 1.0, 1.0, 1.0)
        dcam.Activate(scam)

        # Render OpenGL Cube
        pypangolin.glDrawColouredCube(0.1)

        # Draw camera
        R= [[ 9.99976198e-01,  4.16979787e-04, -6.88693319e-03],
             [-3.91109692e-04,  9.99992865e-01,  3.75732251e-03],
             [ 6.88845077e-03, -3.75453953e-03,  9.99969226e-01]]

        t=[[ 0.02646868],
            [-0.01349629],
            [ 0.99955853]]

        mtx1=[[R[0][0],R[0][1],R[0][2],t[0][0]],
              [R[1][0],R[1][1],R[1][2],t[1][0]],
              [R[2][0],R[2][1],R[2][2],t[2][0]],
              [0,      0,      0,      1     ]]

        print(mtx1)
        gl.glLineWidth(1)
        gl.glColor3f(0.0, 0.0, 1.0)
        DrawCamera(mtx1, 0.5, 0.75, 0.8)
        pypangolin.FinishFrame()



if __name__ == '__main__':
    main()


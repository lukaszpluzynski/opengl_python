import sys
sys.path.append('/Users/lukaszpluzynski/Pangolin/build/src')
import pypangolin
from OpenGL.GL import *

def a_callback():
    print("a pressed")

def Drawsurface():

    glPushMatrix()

    glBegin(GL_LINES)

    '''
    glVertex3f(5,-1,0)
    glVertex3f(5,-1,5)
    glVertex3f(-5,-1,5)
    glVertex3f(-5,-1,0)

    '''
    glVertex3f(5,-1,0)
    glVertex3f(5,-1,5)

    glVertex3f(5,-1,5)
    glVertex3f(-5,-1,5)

    glVertex3f(-5,-1,5)
    glVertex3f(-5,-1,0)

    glVertex3f(-5,-1,0)
    glVertex3f(5,-1,0)

    glEnd()
    glPopMatrix()



def DrawCamera(camera,w=1.0,h_ratio=0.75,z_ratio=0.6):

    h = w * h_ratio
    z = w * z_ratio


    glPushMatrix()

    glBegin(GL_LINES)

    glVertex3f(0,0,0)
    glVertex3f(w,h,z)
    glVertex3f(0,0,0)
    glVertex3f(w,-h,z)
    glVertex3f(0,0,0)
    glVertex3f(-w,-h,z)
    glVertex3f(0,0,0)
    glVertex3f(-w,h,z)

    glVertex3f(w,h,z)
    glVertex3f(w,-h,z)
    glVertex3f(-w,h,z)
    glVertex3f(-w,-h,z)
    glVertex3f(-w,h,z)
    glVertex3f(w,h,z)
    glVertex3f(-w,-h,z)
    glVertex3f(w,-h,z)

    glEnd()

    glPopMatrix()





def main():


    # Utworzenei ramki
    win = pypangolin.CreateWindowAndBind("lukasz jest super", 640, 480)
    glEnable(GL_DEPTH_TEST)



    # USTAWINEI WIDOKU 3d

    # Parametry kamery - macierz kamery
    pm = pypangolin.ProjectionMatrix(640,480,420,420,320,240,0.1,1000)

    # To odpowida za polozenie kamery - orientacje i transalcje widoku
    mv = pypangolin.ModelViewLookAt(0, -10, -8,
                                    0, 0, 0,
                                    0, -1, 0)
    s_cam = pypangolin.OpenGlRenderState(pm, mv)



    ui_width = 180
    handler = pypangolin.Handler3D(s_cam)
    display = pypangolin.CreateDisplay().SetBounds(pypangolin.Attach(0),
                                                 pypangolin.Attach(1),
                                                 pypangolin.Attach.Pix(ui_width),
                                                 pypangolin.Attach(1),
                                                 -640.0/480.0).SetHandler(handler)      #function set hendler set this created space for achiwe mouse and keyboard inpusts


    # UI
    #czyli to jest po prostu panel ja w javie plus ustalone szare tlo jako defaultowe
    pypangolin.CreatePanel("ui").SetBounds(  pypangolin.Attach(0),                      #dolny przyklejony do do
                                             pypangolin.Attach(1),
                                             pypangolin.Attach(0),
                                             pypangolin.Attach.Pix(180))                #prawa krawedz panelu

    var_ui = pypangolin.Var("ui")
    var_ui.A_Button=True
    var_ui.B_Button=True
    var_ui.B_Double=1
    var_ui.B_Str="somethnig"
    var_ui.C_Button = True

    ctrl=-96
    pypangolin.RegisterKeyPressCallback(ctrl+ord('a'), a_callback)

    while not pypangolin.ShouldQuit():
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        display.Activate(s_cam)
        DrawCamera()
        Drawsurface()

        pypangolin.FinishFrame()


if __name__ == "__main__":
    main()

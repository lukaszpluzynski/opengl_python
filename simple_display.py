import sys
sys.path.append('/Users/lukaszpluzynski/Pangolin/build/src')
import OpenGL.GL as gl
import pypangolin


class SetVarFunctor(object):
    def __init__(self, var=None, value=None):
        super().__init__()
        self.var = var
        self.value = value

    def __call__(self):
        self.var.SetVal(self.value)

def reset():
    #float_slider.SetVal(0.5)
    print('You typed ctrl-r or pushed reset')



def main():
    # pypangolin.ParseVarsFile('app.cfg')

    pypangolin.CreateWindowAndBind('Main', 640, 480)
    gl.glEnable(gl.GL_DEPTH_TEST)

    scam = pypangolin.OpenGlRenderState(
        pypangolin.ProjectionMatrix(640, 480, 420, 420, 320, 240, 0.1, 1000),
        pypangolin.ModelViewLookAt(0, 0.5, -3, 0, 0, 0, pypangolin.AxisDirection.AxisY))
    handler3d = pypangolin.Handler3D(scam)

    dcam = pypangolin.CreateDisplay()
    dcam.SetBounds(pypangolin.Attach(0), pypangolin.Attach(1), pypangolin.Attach.Pix(180), pypangolin.Attach(1), -640.0/480.0)
    dcam.SetHandler(handler3d)

    panel = pypangolin.CreatePanel('ui')
    panel.SetBounds(pypangolin.Attach(0),pypangolin.Attach(1),pypangolin.Attach(0),pypangolin.Attach.Pix(180))


    # button = pypangolin.VarValue('ui.Button', value=False, toggle=False)
    # checkbox = pypangolin.VarBool('ui.Checkbox', value=False, toggle=True)
    # float_slider = pypangolin.VarFloat('ui.Float', value=3, min=0, max=5)
    # float_log_slider = pypangolin.VarFloat('ui.Log_scale var', value=3, min=1, max=1e4, logscale=True)
    # int_slider = pypangolin.VarInt('ui.Int', value=2, min=0, max=5)
    # int_slave_slider = pypangolin.VarInt('ui.Int_slave', value=2, toggle=False)
    #
    # save_window = pypangolin.VarBool('ui.Save_Window', value=False, toggle=False)
    # save_cube   = pypangolin.VarBool('ui.Save_Cube',   value=False, toggle=False)
    # record_cube = pypangolin.VarBool('ui.Record_Cube', value=False, toggle=False)

    def reset():
        #float_slider.SetVal(0.5)
        print('You typed ctrl-r or pushed reset')

    # Reset = SetVarFunctor(float_slider, 0.5)
    # reset = pypangolin.VarFunc('ui.Reset', reset)
    # pypangolin.RegisterKeyPressCallback(int(pypangolin.PANGO_CTRL) + ord('r'), reset)      # segfault
    # pypangolin.RegisterKeyPressCallback(int(pypangolin.PANGO_CTRL) + ord('b'), pypangolin.SetVarFunctorFloat('ui.Float', 4.5))      # segfault
    # pypangolin.RegisterKeyPressCallback(int(pypangolin.PANGO_CTRL) + ord('b'), SetVarFunctor(float_slider, 4.5))      # segfault

    while not pypangolin.ShouldQuit():
        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
        gl.glClearColor(1.0, 1.0, 1.0, 1.0)

        # if pypangolin.Pushed(button):
        #     print('You Pushed a button!')
        #
        # if checkbox.Get():
        #     int_slider.SetVal(int(float_slider))
        # int_slave_slider.SetVal(int_slider)
        #
        # if pypangolin.Pushed(save_window):
        #     pypangolin.SaveWindowOnRender("window")
        #
        # if pypangolin.Pushed(save_cube):
        #     pypangolin.SaveWindowOnRender("cube")
        #
        # if pypangolin.Pushed(record_cube):
        #     pypangolin.DisplayBase().RecordOnRender("ffmpeg:[fps=50,bps=8388608,unique_filename]//screencap.avi")


        dcam.Activate(scam)
        gl.glColor3f(1.0, 1.0, 1.0)
        pypangolin.glDrawColouredCube()
        pypangolin.FinishFrame()


if __name__ == '__main__':
    main()
